import waitUntil from 'wait-until-promise'

class AudioPlayer {
  constructor (src) {
    this.audio = new Audio(src)
    this.playing = false

    this.audio.addEventListener('ended', () => {
      this.playing = false
    })
  }

  play () {
    this.playing = true

    return new Promise(resolve => {
      this.audio.play()
      return waitUntil(() => !this.playing, 1000 * 30).then(resolve)
    })
  }

  pause () {
    this.audio.pause()
  }
}

module.exports = AudioPlayer
