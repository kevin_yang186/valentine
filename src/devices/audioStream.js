// https://github.com/mozdevs/mediaDevices-getUserMedia-polyfill/blob/master/mediaDevices-getUserMedia-polyfill.js

const promisifiedOldGUM = (constraints, successCallback, errorCallback) => {
  const getUserMedia = (navigator.getUserMedia ||
                        navigator.webkitGetUserMedia ||
                        navigator.mozGetUserMedia ||
                        navigator.msGetUserMedia)
  if (!getUserMedia) {
    return Promise.reject(new Error('getUserMedia is not implemented in this browser'))
  }
  return new Promise((resolve, reject) => {
    getUserMedia.call(navigator, constraints, resolve, reject)
  })
}

const polyfillGetUserMedia = () => {
  if (navigator.mediaDevices === undefined) {
    navigator.mediaDevices = {}
  }

  if (navigator.mediaDevices.getUserMedia === undefined) {
    navigator.mediaDevices.getUserMedia = promisifiedOldGUM
  }
}

class AudioStream {
  constructor () {
    this.isSupported = true

    polyfillGetUserMedia()
  }

  getAudio () {
    return navigator.mediaDevices
            .getUserMedia({audio: true})
            .then(stream => {
              console.log('getUserMedia supported.')

              return Promise.resolve(stream)
            })
            .catch(error => {
              console.log(`Error: ${error}`)

              this.isSupported = false

              return Promise.reject(error)
            })
  }

  get isMediaSupported () {
    return this.isSupported
  }
}

module.exports = AudioStream
