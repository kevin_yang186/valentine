import AudioStream from './audioStream'
import AudioRecorder from './audioRecorder'
import AudioPlayer from './audioPlayer'

const audioPlayer = src => new AudioPlayer(src)

module.exports = {
  AudioStream,
  AudioRecorder,
  audioPlayer
}
