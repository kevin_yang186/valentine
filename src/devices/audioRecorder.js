import PolyfillMediaRecorder from 'audio-recorder-polyfill'

const polyfill = () => {
  if (!window.MediaRecorder) {
    window.MediaRecorder = PolyfillMediaRecorder
  }
}

class AudioRecorder {
  constructor () {
    polyfill()
  }

  get isSupported () {
    return !!window.MediaRecorder || !PolyfillMediaRecorder.notSupported
  }

  record (stream) {
    this.mediaRecorder = new MediaRecorder(stream)

    return new Promise((resolve, reject) => {
      if (!this.isSupported) {
        return reject(new Error('AudioRecorder is not supported'))
      }

      this.mediaRecorder.addEventListener('dataavailable', e =>
               resolve({ blob: e.data, url: URL.createObjectURL(e.data)})
            )
    })
  }

  start () {
    if (this.mediaRecorder.state === 'inactive') {
      this.mediaRecorder.start()
    }
  }

  stop () {
    if (this.mediaRecorder.state === 'recording') {
      this.mediaRecorder.stop()
    }
  }
}

module.exports = AudioRecorder
