import Recorder from './recorder'
import { search } from './http'

class Application {
  constructor (options) {
    this.recorder = new Recorder(options)
  }

  startRecord () {
    this.recorder.start()
  }

  stopRecord () {
    this.recorder.stop().then(data => {
      this.data = data

      return data
    })
  }

  play () {
    return this.recorder.play()
  }

  pause () {
    this.recorder.pause()
  }

  create (serialNo, orderId) {
    return this.recorder.create(this.data, serialNo, orderId)
  }

  upload (orderId) {
    return this.recorder.upload(this.data, orderId)
  }

  query (text) {
    return new Promise((resolve, reject) => {
      return search(text).then(response => {
        if (response.data.statusCode === 200) {
          return resolve(response.data.body)
        } else {
          return reject(response.data)
        }
      })
    })
  }

  test (url) {
    this.recorder.test(url)
  }
}

window.RecorderApplication = Application
