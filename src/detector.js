class Detector {
  isWeChat () {
    const userAgent = window.navigator.userAgent.toLowerCase()

    return userAgent.indexOf('micromessenger') > 0
  }
}

module.exports = Detector
