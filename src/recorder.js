import { H5AudioRecorder, WeChatAudioRecorder} from './adapters'
import Detector from './detector'

class Recorder {
  constructor (options) {
    this.detector = new Detector()

    if (this.detector.isWeChat()) {
      this.recorder = new WeChatAudioRecorder(options)
    } else {
      this.recorder = new H5AudioRecorder()
    }
  }

  get isSupported () {
    return this.recorder.isSupported
  }

  start () {
    if (this.isSupported) {
      this.recorder.start()
    }
  }

  error () {
    if (this.detector.isWeChat()) {
      return Promise.reject(new Error('WeChat library is not initialised.'))
    } else {
      return Promise.reject(new Error('H5 Media not supported.'))
    }
  }

  stop () {
    if (this.isSupported) {
      return this.recorder.stop()
    } else {
      return this.error()
    }
  }

  play () {
    if (this.isSupported) {
      return this.recorder.play()
    }
  }

  pause () {
    if (this.isSupported) {
      this.recorder.pause()
    }
  }

  upload (data, orderId) {
    return this.recorder.upload(data, orderId)
  }

  create (data, serialNo, orderId) {
    return this.recorder.create(data, serialNo, orderId)
  }

  test (url) {
    return this.recorder.test(url)
  }
}

module.exports = Recorder
