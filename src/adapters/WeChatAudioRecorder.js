import axios from 'axios'
import { audioPlayer } from '../devices'
import BaseAudioRecorder from './BaseAudioRecorder'
import waitUntil from 'wait-until-promise'
import { signatureUrl, wechatMediaUrl } from '../config'

class WeChatAudioRecorder extends BaseAudioRecorder {
  constructor (options) {
    super()

    this.initialized = false

    axios.get(`${signatureUrl}/${btoa(options.currentUrl)}`)
         .then(response => {
           wx.config(response.data.sign_nature)

           this.initialized = true
           this.playing = false

           this.events()
         })
  }

  events() {
    wx.ready(() => {
      wx.onVoicePlayEnd({
        success: () => {
          this.playing = false
        }
      })
    })
  }

  start () {
    wx.ready(() => {
      this.localId = null

      wx.startRecord()
    })
  }

  getResource (serverId) {
    return axios.get(`${wechatMediaUrl}/${serverId}`)
                .then(response => `data:audio/amr;base64,${response.data.content}`)
  }

  stop () {
    return new Promise(resolve => {
      wx.ready(() => {
        wx.stopRecord({
          success: response => {
            this.localId = response.localId

            wx.uploadVoice({
              localId: response.localId,
              isShowProgressTips: 1,
              success: response => this.getResource(response.serverId)
                                       .then(resolve)
            })
          }
        })
      })
    })
  }

  play () {
    return new Promise(resolve => {
      wx.ready(() => {
        this.playing = true
        wx.playVoice({ localId: this.localId })

        return waitUntil(() => !this.playing, 1000 * 30).then(resolve)
      })
    })
  }

  pause () {
    wx.ready(() => {
      wx.pauseVoice({ localId: this.localId })
    })
  }

  upload (data, orderId) {
    return this.doUpload(data, orderId, 'amr')
  }

  create (data, serialNo, orderId) {
    return this.doCreate(data, serialNo, orderId, 'amr')
  }

  test (url) {
    wx.ready(() => {
      audioPlayer(url).play()
    })
  }

  get isSupported () {
    return this.initialized
  }
}

module.exports = WeChatAudioRecorder
