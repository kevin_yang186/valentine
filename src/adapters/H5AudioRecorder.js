import waitUntil from 'wait-until-promise'
import { AudioStream, AudioRecorder } from '../devices'
import { audioPlayer } from '../devices'
import BaseAudioRecorder from './BaseAudioRecorder'

class H5AudioRecorder extends BaseAudioRecorder {
  constructor () {
    super()

    this.audioStream = new AudioStream()
    this.audioRecorder = new AudioRecorder()
  }

  get isSupported () {
    return this.audioStream.isSupported && this.audioRecorder.isSupported
  }

  start () {
    if (this.isSupported) {
      this.reset()

      this.audioStream.getAudio().then(stream => {
        this.audioRecorder.record(stream).then(data => {
          this.blob = data.blob
          this.audioPlayer = audioPlayer(data.url)
        })
        this.audioRecorder.start()
      })
    }
  }

  readBlob (blob) {
    return new Promise(resolve => {
      const reader = new FileReader()

      reader.readAsDataURL(blob)
      reader.onloadend = () => resolve(reader.result)
    })
  }

  reset () {
    this.blob = null
    this.audioPlayer = null
  }

  stop () {
    if (this.isSupported) {
      this.audioRecorder.stop()

      return waitUntil(() => this.blob).then(blob => this.readBlob(blob))
    }
  }

  play () {
    return waitUntil(() => this.audioPlayer).then(player => player.play())
  }

  pause () {
    return waitUntil(() => this.audioPlayer).then(player => {
      player.pause()
    })
  }

  upload (data, orderId) {
    return this.doUpload(data, orderId, 'webm')
  }

  create (data, serialNo, orderId) {
    return this.doCreate(data, serialNo, orderId, 'webm')
  }

  test (url) {
    audioPlayer(url).play()
  }
}

module.exports = H5AudioRecorder
