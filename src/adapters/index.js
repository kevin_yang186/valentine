import H5AudioRecorder from './H5AudioRecorder'
import WeChatAudioRecorder from './WeChatAudioRecorder'

module.exports = {
  H5AudioRecorder,
  WeChatAudioRecorder
}
