import http from '../http'

class BaseAudioRecorder {
  doUpload (data, orderId, type) {
    if (data) {
      return http.upload(data, orderId, type)
    } else {
      return Promise.reject(new Error('No data stream'))
    }
  }

  doCreate (data, serialNo, orderId, type) {
    if (data) {
      return http.create(data, serialNo, orderId, type)
    } else {
      return Promise.reject(new Error('No data stream'))
    }
  }
}

module.exports = BaseAudioRecorder
