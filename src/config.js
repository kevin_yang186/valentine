module.exports = {
  signatureUrl: 'https://wechat.danielwellington.cn/get_val_day_sign_nature',
  wechatMediaUrl: 'https://wechat.danielwellington.cn/get_val_day_wechat_resource',
  uploadUrl: 'https://dev-valentine-wechat.danielwellington.cn/rest/upload',
  updateUrl: 'https://dev-valentine-wechat.danielwellington.cn/rest/add',
  getAudioUrl: 'https://dev-valentine-wechat.danielwellington.cn/rest/getMedia'
}
