import axios from 'axios'
import { uploadUrl, updateUrl, getAudioUrl } from './config'

const updateSerialNo = (serialNo, mediaName) => axios.post(updateUrl, { mediaName: mediaName, serialNumber: serialNo })

const upload = (data, orderId, type = 'webm') => axios.post(uploadUrl, { contentType: type, mediaStream: data, orderId })

const create = (data, serialNo, orderId, type = 'webm') => upload(data, orderId, type)
                        .then(response => {
                          const body = JSON.parse(response.data.body)
                          return updateSerialNo(serialNo, body.key)
                        })

const search = serialNo => axios.post(getAudioUrl, { serialNumber: serialNo })

module.exports = {
  create,
  upload,
  search
}
